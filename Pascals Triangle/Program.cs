﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Pascals_Triangle
{
    class Program
    {
        private const int RowsToGenerate = 30;
        private const string OutputFileLocation = @"C:\tmp\pascal.txt";

        static void Main(string[] args)
        {
            var rows = Enumerable.Range(0, RowsToGenerate);
            var triangle = rows
                .Select(row => Enumerable.Range(0, row + 1)
                .Select(column => GetPascalValue(row, column)));

            using (var writer = new StreamWriter(OutputFileLocation))
            {
                foreach (var row in triangle)
                {
                    writer.WriteLine(string.Join(" ", row));
                }
            }
        }

        static int GetPascalValue(int row, int column)
        {
            if (column == 0 || row == 0 || column == row)
            {
                return 1;
            }
            else
            {
                // (Formula from : https://en.wikipedia.org/wiki/Pascal%27s_triangle#Calculating_a_row_or_diagonal_by_itself)
                return (GetPascalValue(row, column - 1) * (row + 1 - column)) / column;
            }
        }
    }
}
